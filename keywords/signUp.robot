*** Settings ***
Documentation    Sign up page all veriables and keywords
Library         SeleniumLibrary
Library         Collections
Library         String
Library         OperatingSystem
Library         BuiltIn
*** Variables ***
${browser}                           Chrome    
${magento_url}                       http://www.wellevate.me/index.php/admin
${SIGN UP}                           xpath=//span[contains(text(),'Practitioner Sign Up')]
${PRACTITONER FIRST NAME}            xpath=//input[@id='firstName']
${PRACTITONER LAST NAME}             xpath=//input[@id='lastName']
${TYPE OF PRACTITONER}               xpath=//md-input-container[@class='md-select-container md-input-has-placeholder']
${EMAIL BOX}                         xpath=//input[@id='email']
${PASSWORDq}                         xpath=//input[@id='password']
${TERMS AND USE LINK}                xpath=//div[@class='md-icon']
${CONTINUE}                          xpath=//span[contains(text(),'Continue')]
${PRACTITONER & PATIENT TEXT}        xpath=//h2[@class='text-center pt2 pb2']
${ADDRESS TEXTBOX}                   xpath=//input[@name='address']
${APARTMENT TEXTBOX}                 xpath=//input[@name='address2']
${CITY TEXTBOX}                      xpath=//input[@name='city']
${STATE TEXTBOX}                     xpath=//md-select[@name='state']
${ZIP TEXTBOX}                       xpath=//input[@name='postal']
${BUSINESS TEXBOX}                   xpath=//input[@name='phone']
${FINISH BUTTON}                     xpath=//button[@id='saveSLStorefrontBtn']
${GOTiT BUTTON}                      xpath=//button[@class='dc-button dc-primary md-button md-ink-ripple']
${MY ACCOUNT}                        xpath=//i[@class='fa fa-lg fa-angle-down']
${LOG IN BUTTON}                     xpath=//ul[@class='nav navbar-nav navbar-right nav-right layout-fill layout-align-end-start layout-row']//a[@id='login-dialog']
${LOG IN BUTTON 2}                   xpath=//button[@class='btn btn-primary btn-w-lg input-form']

${excel}    ../testData/signUp.xlsx  

*** Keywords ***
Verify Next Landing Page Is LogIn Page
    ${Text}    Get Text    xpath=//h2[@class='text-center pt2 pb2']
    Should Be Equal As Strings    ${Text}    Practitioner & Patient Log In    
LogIn By Using Registered Email
    [Arguments]    ${UserName}    ${Password}
    Wait Until Element Is Visible    ${EMAIL BOX}    
    Input Text    ${EMAIL BOX}    ${UserName}
    Wait Until Element Is Visible    ${PASSWORDq} 
    Input Password    ${PASSWORDq}    ${Password}       
    Click Element    ${LOG IN BUTTON 2}  
    ${Title}    Get Title  
    Convert To String    ${Title}
    Should Be Equal As Strings    ${Title}    Professional Supplement Dispensary | Wellevate

Click On logIn Button
    [Documentation]
    Wait Until Element Is Visible    ${LOG IN BUTTON} 
    Click Element    ${LOG IN BUTTON}       

Click On Got it And Close PopUp
    [Documentation]
    Wait Until Element Is Visible    ${GOTiT BUTTON}    
    Click Button    ${GOTiT BUTTON}    

Select My Account DropDown And Select Action
    [Arguments]    ${Action}
    [Documentation]   
    Wait Until Element Is Visible    ${MY ACCOUNT}
    Click Element    ${MY ACCOUNT}
    Click Element    xpath=//span[contains(text(),'${Action}')]    
Launch URL
    [Documentation]
    Set Selenium Speed	    0.5 seconds
    Open Browser                        ${magento_url}    ${browser} 
    Maximize Browser Window   
    Set Browser Implicit Wait           15
    ${Title}    Get Title
    Convert To String    ${Title}
    Should Be Equal    ${Title}    	Professional Supplement Dispensary | Wellevate     

Click On Sign Up And Submit Completely
    [Documentation]
    [Arguments]    ${firstNM}    ${LastNm}    ${Type of Practitoners}    ${email}    ${Password}
    Wait Until Element Is Visible    ${SIGN UP}    
    Click Element    ${SIGN UP}  
    Wait Until Element Is Visible     ${PRACTITONER FIRST NAME}
    Input Text    ${PRACTITONER FIRST NAME}    ${firstNM}  
    Wait Until Element Is Visible    ${PRACTITONER LAST NAME}
    Input Text    ${PRACTITONER LAST NAME}    ${LastNm}
    Click Element     ${TYPE OF PRACTITONER}   
    Click Element    xpath=//div[contains(text(),'${Type of Practitoners}')]   
    Input Text    ${EMAIL BOX}    ${email}
    Click Element    ${PASSWORDq}
    Input Text    ${PASSWORDq}    ${Password}
    Press Keys    ${TERMS AND USE LINK}    Tab
    Press Keys    ${TERMS AND USE LINK}    Tab
    Wait Until Element Is Visible    ${CONTINUE}
    Click Element    ${CONTINUE}  
    
Verify Practitioner & Patient Log In Text
    [Documentation]       
    Wait Until Element Is Visible    ${PRACTITONER & PATIENT TEXT}    
    ${Text}    Get Element Text    Practitioner & Patient Log In
    Convert To String    ${Text}
    Should Be Equal As Strings    ${Text}    Practitioner & Patient Log In 

Fill Up All The Text Box Inside The About your practice 
    [Documentation]
    [Arguments]    ${state}
    Wait Until Element Is Enabled    ${ADDRESS TEXTBOX}    
    Input Text    ${ADDRESS TEXTBOX}    14456 87th st
    Wait Until Element Is Enabled    ${APARTMENT TEXTBOX}
    Input Text    ${APARTMENT TEXTBOX}    ${EMPTY}
    Wait Until Element Is Enabled    ${CITY TEXTBOX}
    Input Text    ${CITY TEXTBOX}    Jamaica
    Wait Until Element Is Enabled    ${STATE TEXTBOX}
    Click Element    ${STATE TEXTBOX}    
    Click Element    //div[text()='${state}']    
    Wait Until Element Is Enabled    ${ZIP TEXTBOX}
    Input Text    ${ZIP TEXTBOX}    11432
    Wait Until Element Is Enabled    ${BUSINESS TEXBOX}
    Input Text    ${BUSINESS TEXBOX}    9294002210
    Wait Until Element Is Enabled    ${FINISH BUTTON}
    Click Element    ${FINISH BUTTON}
